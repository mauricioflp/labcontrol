extern "C" {
#include "user_interface.h"
}

#include <IRremoteESP8266.h>
#include <IRsend.h>
//#include <PubSubClient.h>
#include <ESP8266WiFi.h>


//const char* BROKER_MQTT = "192.168.0.105"; // ip/host do broker
//int BROKER_PORT = 1883; // porta do broker

#include "DHT.h"
#define DHTPIN 2     // Pino de Leitura
#define DHTTYPE DHT22 

DHT dht(DHTPIN, DHTTYPE);

//const char* ssid = "CAP";
//const char* password = "arduino2017";

const char* ssid = "Coelhinho";
const char* password = "52686719";




//IPAddress ip(10,0,0,182); 
//IPAddress gateway(10,0,0,1);
//IPAddress subnet(255,255,255,0);

IPAddress ip(192,168,0,182); 
IPAddress gateway(192,168,0,254);
IPAddress subnet(255,255,255,0);
 
WiFiServer server(80);


//PubSubClient MQTT(espClient); // instancia o mqtt
//  WiFiClient espClient = server.available();
//  PubSubClient MQTT(espClient);

  float humid;
  float temp;

  int intHumid;
  int intTemp;

  int Lamp1 = 14;
  int Lamp2 = 12;
  int Lamp3 = 13;
  int Lamp4 = 15;

  IRsend irsend1(16);
  IRsend irsend2(5);
  IRsend irsend3(4);
  IRsend irsend4(0);
  
  float ldrValorSend;

  bool ligado1 = true;
  bool ligado2 = true;
  bool ligado3 = true;
  bool ligado4 = true;

  //SINAIS INFRAVERMELHO
  short unsigned int Desligar[197] = {6152,7360,592,1612,588,1616,588,1616,560,1644,560,1640,564,1620,588,1612,592,1612,564,580,564,552,584,556,556,556,588,552,560,552,592,552,588,552,560,1612,596,1612,588,1616,588,1612,564,1644,560,1612,596,1612,588,1616,588,552,564,548,592,552,560,548,596,552,584,556,556,556,588,552,560,1644,560,1644,564,1612,592,1612,588,1612,592,1616,560,1644,564,1612,592,552,560,576,564,552,588,552,560,552,592,552,560,580,564,552,584,1612,568,1636,564,1644,564,548,592,552,564,1640,564,548,588,1612,564,580,564,548,592,552,560,1616,592,1612,588,548,568,1636,564,552,592,1612,592,1612,588,552,564,1616,588,1612,592,552,560,1644,564,1612,588,548,564,580,564,1612,592,548,592,552,564,1612,588,548,592,552,564,548,592,1612,592,552,564,1612,588,552,588,1612,564,556,588,548,592,1612,564,552,588,1616,588,548,564,1640,568,548,592,1612,592,1612,564,7380,592};
  short unsigned int Ligar20C[197] = {6172,7336,588,1612,592,1616,588,1616,560,1644,560,1644,564,1612,592,1612,564,1644,560,548,596,552,560,580,560,552,564,580,560,552,588,556,560,580,560,1612,588,1620,584,1620,584,1616,560,1644,568,1612,588,1616,588,1612,592,552,564,552,588,552,560,552,592,552,560,580,564,548,588,556,560,1636,568,1616,584,1620,588,1612,592,1612,560,1644,564,1612,592,1612,592,552,560,552,592,552,560,552,592,548,588,552,568,548,588,552,560,1640,564,552,592,1612,592,548,564,552,592,1612,592,548,564,1612,592,552,592,1608,564,552,588,1616,588,1616,588,548,568,1616,588,548,592,1612,564,1640,564,552,592,1608,596,1612,560,552,592,1612,592,1612,592,552,564,548,592,1612,588,548,572,548,588,1616,588,548,568,552,588,548,560,1644,564,548,596,1608,596,548,564,1612,592,548,592,548,568,1608,596,548,592,1612,560,552,596,1608,592,548,564,1640,568,1612,592,7380,588};
  short unsigned int Temp16C[197] = {6160,7328,592,1644,564,1616,592,1612,592,1612,564,1612,596,1612,596,1608,592,1616,560,552,592,552,564,548,592,548,592,552,564,552,592,552,560,552,592,1612,592,1612,564,1644,564,1612,592,1616,592,1608,568,1640,564,1616,592,548,564,552,592,548,592,552,564,548,592,552,560,552,592,548,592,524,592,548,596,1608,568,1640,564,1612,596,1612,592,1612,560,1648,564,1612,592,1612,596,544,540,576,592,552,564,548,592,552,560,556,592,1608,596,548,560,1644,564,552,592,548,560,1620,596,544,592,1612,564,552,596,1612,592,544,536,1648,592,1612,592,548,560,1644,564,552,592,1612,588,1616,564,1612,600,1612,592,1612,588,520,600,1612,592,1612,592,548,568,544,596,548,560,556,592,548,588,1616,536,580,592,548,536,576,600,1608,596,544,536,1644,592,548,592,1612,564,552,592,548,592,1612,540,576,596,1608,592,524,592,1612,592,552,588,1616,536,1640,596,7360,612};
  short unsigned int Temp17C[197] = {6148,7364,584,1616,592,1612,588,1612,568,1640,564,1616,588,1612,596,1612,588,1612,564,552,596,548,592,524,592,548,588,548,568,548,596,548,560,556,592,1612,588,1612,592,1616,560,1640,568,1612,592,1612,596,1604,564,1644,564,552,592,548,564,552,588,552,592,548,568,548,592,548,564,552,592,1612,592,548,560,1620,592,1612,588,1612,592,1612,564,1640,568,1616,588,548,592,1612,564,552,592,548,564,576,568,544,596,548,564,552,592,1612,592,544,568,1612,596,544,596,548,568,1612,588,552,560,1640,564,552,596,1608,592,548,564,1616,592,1612,592,548,564,1640,564,552,592,548,564,1640,568,1612,592,1612,588,1616,588,532,584,1612,596,1608,592,1612,564,552,596,544,592,548,568,552,588,1612,596,544,568,548,592,548,564,1640,568,548,592,1612,560,556,592,1608,596,548,564,552,588,1612,596,544,564,1640,572,544,592,1612,592,528,588,1616,588,1608,596,7380,592};
  short unsigned int Temp18C[197] = {6152,7336,612,1612,568,1640,564,1616,592,1612,588,1612,596,1612,564,1640,568,1612,588,552,564,552,592,548,592,548,568,548,588,548,564,552,596,548,564,1640,564,1616,588,1612,596,1608,596,1612,564,1636,568,1612,596,1612,592,548,564,552,588,548,568,548,592,548,596,548,568,548,588,548,564,1624,584,548,596,1612,564,1636,572,1608,592,1612,596,1612,592,1608,564,556,592,1608,596,548,564,552,588,548,564,556,588,548,596,524,588,1616,588,548,564,1640,564,552,596,548,564,1636,564,552,592,1612,596,548,564,1612,592,544,596,1612,564,1616,592,548,592,1608,564,560,588,1612,592,548,564,1636,568,1616,592,1608,596,548,564,1636,568,1612,592,548,564,1644,564,552,588,548,564,556,588,1612,596,544,564,552,592,552,588,1612,564,552,592,1612,592,552,556,1624,588,544,596,548,564,1616,588,552,588,1612,564,552,596,1612,592,548,564,1616,588,1612,596,7352,620};
  short unsigned int Temp19C[197] = {6160,7352,596,1612,588,1612,564,1644,568,1612,592,1612,592,1608,568,1640,536,1644,592,548,564,576,564,548,596,548,564,552,592,548,564,552,588,552,592,1612,560,1644,568,1612,588,1612,596,1612,564,1640,564,1616,588,1612,596,548,564,552,592,548,588,528,592,544,596,548,564,548,596,548,564,1636,568,548,592,1612,592,1616,564,1616,588,1612,592,1612,596,1608,568,552,588,1612,596,548,560,552,596,548,564,552,588,548,592,548,564,1616,588,548,596,1612,564,552,592,548,588,1616,560,556,592,1612,592,548,564,1616,588,548,596,1608,564,1644,564,552,588,1612,592,552,564,548,596,548,564,1636,568,1612,592,1612,596,548,564,1616,588,1612,596,1608,592,1612,568,552,588,548,564,552,592,1612,596,548,564,552,588,548,592,1612,564,552,592,1612,592,548,564,1620,588,548,592,548,568,1612,588,548,596,1612,564,552,592,1612,588,548,564,1640,568,1612,596,7376,596};
  short unsigned int Temp20C[197] = {6152,7360,588,1612,596,1612,564,1636,564,1616,592,1616,592,1612,588,1612,564,1620,592,548,588,548,572,548,592,548,560,552,596,548,564,552,592,548,588,1612,564,1644,564,1616,588,1612,596,1612,592,1612,564,1620,584,1616,592,548,564,552,592,548,592,548,564,548,596,548,564,552,592,548,564,1636,568,548,596,1612,588,1612,564,1644,564,1612,596,1612,588,1612,564,552,596,1612,592,548,564,552,588,548,596,524,588,548,596,548,564,1616,588,548,596,1608,564,552,596,548,588,1612,564,552,592,1612,592,548,564,1620,588,548,596,1608,564,1640,564,552,592,1612,592,552,564,1612,592,1608,596,548,564,1616,592,1612,592,548,564,1640,568,1612,592,548,588,532,584,1612,596,548,564,552,592,1612,588,548,564,552,596,548,588,1612,564,556,588,1612,596,548,564,1612,596,548,588,548,564,1616,592,548,592,1612,564,556,588,1612,596,544,564,1616,592,1612,592,7380,596};

  int ldrPin = A0;  //DEFINE O PINO NO QUAL O SENSOR DE LUMINOSIDADE LDR ESTARA CONECTADO ADC0 OU A0 NO NODEMCU
  int ldrValor = 0; //VALOR LIDO DO SENSOR DE LUMINOSIDADE LDR

  int rele1 = 15; //DEFINE O PINO NO QUAL A PORTA 1 DO RELÉ ESTARA CONECTADO: GPIO-15 OU D8 NO NODEMCU
  int estadorele1;

  os_timer_t mTimer;

  bool _timeout = false;



/* MQTT */

// Funcão para se conectar ao Broker MQTT
void initMQTT() {
//  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
//  MQTT.setCallback(mqtt_callback);
}

//Função que recebe as mensagens publicadas
void mqtt_callback(char* topic, byte* payload, unsigned int length) {

  String message;
  for (int i = 0; i < length; i++) {
    char c = (char)payload[i];
    message += c;
  }
  Serial.println("Tópico => " + String(topic) + " | Valor => " + String(message));
  if (message == "1") {
    digitalWrite(D5, 1);
  } else {
    digitalWrite(D5, 0);
  }
  Serial.flush();
}

void reconnectMQTT() {
/*  while (!MQTT.connected()) {
    Serial.println("Tentando se conectar ao Broker MQTT: " + String(BROKER_MQTT));
    if (MQTT.connect("ESP8266-ESP12-E")) {
      Serial.println("Conectado");
      MQTT.subscribe("DZ/esp8266-01/led");

    } else {
      Serial.println("Falha ao Reconectar");
      Serial.println("Tentando se reconectar em 2 segundos");
      delay(2000);
    }
  }*/
}




//Nunca execute nada na interrupcao, apenas setar flags!
void tCallback(void *tCall){
    _timeout = true;
}

void usrInit(void){
    os_timer_setfn(&mTimer, tCallback, NULL);
    os_timer_arm(&mTimer, 5000, true);
}

void setup(){
  Serial.begin(115200);
  Serial.println("Iniciando");
  delay(10);

  pinMode(Lamp1, OUTPUT);
  digitalWrite(Lamp1, 0);
  
  pinMode(Lamp2, OUTPUT);
  digitalWrite(Lamp2, 0);

  pinMode(Lamp3, OUTPUT);
  digitalWrite(Lamp3, 0);

  pinMode(Lamp4, OUTPUT);
  digitalWrite(Lamp4, 0);

  WiFi.begin(ssid, password);
  WiFi.config(ip, gateway, subnet);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  server.begin();

  irsend1.begin();
  irsend2.begin();
  irsend3.begin();
  irsend4.begin();
  
  Serial.println("DHT22 Iniciado!");
  dht.begin();  // Inicia sensor de temperatura e humidade DHTxx
  Serial.println("Interrupcao inciada");
  usrInit();    //INICIA A PRIMEIRA INTERRUPCAO
  pinMode(rele1, OUTPUT);
  
  
  
}

void loop() {

/*  if (!MQTT.connected()) {
    reconnectMQTT();
  }
*/
//========================================================================================================
  //Temperatura, Umidade e Luminosidade
  if (_timeout)
  {
    //Luminosidade
    ldrValor = analogRead(ldrPin); //O VALOR LIDO SERA ENTRE 0 E 1023
    ldrValor = map(ldrValor, 0, 1023, 100, 0);
    Serial.print("Luminosidade: ");
    Serial.print(ldrValor);
    Serial.print("% ");

    ldrValorSend = (float)ldrValor;
    ldrValorSend = ldrValorSend / 100;
    //Serial.print(ldrValorSend);

    //Temperatura, Umidade
    humid = dht.readHumidity();
    temp = dht.readTemperature();
    if (isnan(temp) || isnan(humid)) 
    {
      Serial.println("Failed to read from DHT");
    } 
    else
    {
      Serial.print("Umidade: ");
      intHumid = (int)humid;
      Serial.print(intHumid);
      Serial.print("% ");
      Serial.print("Temperatura: ");
      intTemp = (int)temp;
      Serial.print(intTemp);
      Serial.println("*C");
    }
    _timeout = false;
  }

  //======================================================================================================
  //Web Requests
  WiFiClient client = server.available();
  
  if (!client) {
    return;
  }
  
  String req = client.readStringUntil('\r');
  Serial.println(req);

  client.flush();

  String buf = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";

  client.print(buf);

  client.flush();

  if (req.indexOf("Temperatura") != -1)
  {
    client.flush();
    client.print(intTemp);
  }
  else  if (req.indexOf("Umidade") != -1)
  {
    client.flush();
    client.print(intHumid);
  }
  else if (req.indexOf("Luminosidade") != -1)
  {
    client.flush();
    client.print(ldrValorSend);
  }
  else if (req.indexOf("LigarL1") != -1)
  {
    if (ligado1)
    {
      digitalWrite(Lamp1, 1);
      ligado1 = false;
      client.flush();
    }
    else
    {
      digitalWrite(Lamp1, 0);
      ligado1 = true;
      client.flush();
    }
  }
  else if (req.indexOf("LigarL2") != -1)
  {
    if (ligado2)
    {
      digitalWrite(Lamp2, 1);
      ligado2 = false;
      client.flush();
    }
    else
    {
      digitalWrite(Lamp2, 0);
      ligado2 = true;
      client.flush();
    }
  }
  else if (req.indexOf("LigarL3") != -1)
  {
    if (ligado3)
    {
      digitalWrite(Lamp3, 1);
      ligado3 = false;
      client.flush();
    }
    else
    {
      digitalWrite(Lamp3, 0);
      ligado3 = true;
      client.flush();
    }
  }
  else if (req.indexOf("LigarL4") != -1)
  {
    if (ligado4)
    {
      digitalWrite(Lamp4, 1);
      ligado4 = false;
      client.flush();
    }
    else
    {
      digitalWrite(Lamp4, 0);
      ligado4 = true;
      client.flush();
    }
  }
  //Sinais AR CONDICIONADO
  //AR1=======================================================================
  else if (req.indexOf("Ar1") != -1)
  {
    if (req.indexOf("Ar1Desligar") != -1)
    {
      irsend1.sendRaw(Desligar, 197,38);  
      Serial.println("Comando enviado: Desliga");
    }
    else if (req.indexOf("Ar1Ligar") != -1)
    { 
      irsend1.sendRaw(Ligar20C, 197,38);
      Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
    }
    else if (req.indexOf("Ar1Temp16C") != -1)
    { 
      irsend1.sendRaw(Temp16C, 197,38);
      Serial.println("Comando enviado: Temperatura a 16°C");
    }
    else if (req.indexOf("Ar1Temp17C") != -1)
    { 
      irsend1.sendRaw(Temp17C, 197,38);
      Serial.println("Comando enviado: Temperatura a 17°C");
    }
    else if (req.indexOf("Ar1Temp18C") != -1)
    { 
      irsend1.sendRaw(Temp18C, 197,38);
      Serial.println("Comando enviado: Temperatura a 18°C");
    }
    else if (req.indexOf("Ar1Temp19C") != -1)
    { 
      irsend1.sendRaw(Temp19C, 197,38);
      Serial.println("Comando enviado: Temperatura a 19°C");
    }
    else if (req.indexOf("Ar1Temp20C") != -1)
    { 
      irsend1.sendRaw(Temp20C, 197,38);
      Serial.println("Comando enviado: Temperatura a 20°C");
    }
  }
  //AR2=======================================================================
    else if (req.indexOf("Ar2") != -1)
  {
    if (req.indexOf("Ar2Desligar") != -1)
    {
      irsend2.sendRaw(Desligar, 197,38);  
      Serial.println("Comando enviado: Desliga");
    }
    else if (req.indexOf("Ar2Ligar") != -1)
    { 
      irsend2.sendRaw(Ligar20C, 197,38);
      Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
    }
    else if (req.indexOf("Ar2Temp16C") != -1)
    { 
      irsend2.sendRaw(Temp16C, 197,38);
      Serial.println("Comando enviado: Temperatura a 16°C");
    }
    else if (req.indexOf("Ar2Temp17C") != -1)
    { 
      irsend2.sendRaw(Temp17C, 197,38);
      Serial.println("Comando enviado: Temperatura a 17°C");
    }
    else if (req.indexOf("Ar2Temp18C") != -1)
    { 
      irsend2.sendRaw(Temp18C, 197,38);
      Serial.println("Comando enviado: Temperatura a 18°C");
    }
    else if (req.indexOf("Ar2Temp19C") != -1)
    { 
      irsend2.sendRaw(Temp19C, 197,38);
      Serial.println("Comando enviado: Temperatura a 19°C");
    }
    else if (req.indexOf("Ar2Temp20C") != -1)
    { 
      irsend2.sendRaw(Temp20C, 197,38);
      Serial.println("Comando enviado: Temperatura a 20°C");
    }
  }
  //AR3=======================================================================
    else if (req.indexOf("Ar3") != -1)
  {
    if (req.indexOf("Ar3Desligar") != -1)
    {
      irsend3.sendRaw(Desligar, 197,38);  
      Serial.println("Comando enviado: Desliga");
    }
    else if (req.indexOf("Ar3Ligar") != -1)
    { 
      irsend3.sendRaw(Ligar20C, 197,38);
      Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
    }
    else if (req.indexOf("Ar3Temp16C") != -1)
    { 
      irsend3.sendRaw(Temp16C, 197,38);
      Serial.println("Comando enviado: Temperatura a 16°C");
    }
    else if (req.indexOf("Ar3Temp17C") != -1)
    { 
      irsend3.sendRaw(Temp17C, 197,38);
      Serial.println("Comando enviado: Temperatura a 17°C");
    }
    else if (req.indexOf("Ar3Temp18C") != -1)
    { 
      irsend3.sendRaw(Temp18C, 197,38);
      Serial.println("Comando enviado: Temperatura a 18°C");
    }
    else if (req.indexOf("Ar3Temp19C") != -1)
    { 
      irsend3.sendRaw(Temp19C, 197,38);
      Serial.println("Comando enviado: Temperatura a 19°C");
    }
    else if (req.indexOf("Ar3Temp20C") != -1)
    { 
      irsend3.sendRaw(Temp20C, 197,38);
      Serial.println("Comando enviado: Temperatura a 20°C");
    }
  }
  //AR4=======================================================================
    else if (req.indexOf("Ar4") != -1)
  {
    if (req.indexOf("Ar4Desligar") != -1)
    {
      irsend4.sendRaw(Desligar, 197,38);  
      Serial.println("Comando enviado: Desliga");
    }
    else if (req.indexOf("Ar4Ligar") != -1)
    { 
      irsend4.sendRaw(Ligar20C, 197,38);
      Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
    }
    else if (req.indexOf("Ar4Temp16C") != -1)
    { 
      irsend4.sendRaw(Temp16C, 197,38);
      Serial.println("Comando enviado: Temperatura a 16°C");
    }
    else if (req.indexOf("Ar4Temp17C") != -1)
    { 
      irsend4.sendRaw(Temp17C, 197,38);
      Serial.println("Comando enviado: Temperatura a 17°C");
    }
    else if (req.indexOf("Ar4Temp18C") != -1)
    { 
      irsend4.sendRaw(Temp18C, 197,38);
      Serial.println("Comando enviado: Temperatura a 18°C");
    }
    else if (req.indexOf("Ar4Temp19C") != -1)
    { 
      irsend4.sendRaw(Temp19C, 197,38);
      Serial.println("Comando enviado: Temperatura a 19°C");
    }
    else if (req.indexOf("Ar4Temp20C") != -1)
    { 
      irsend4.sendRaw(Temp20C, 197,38);
      Serial.println("Comando enviado: Temperatura a 20°C");
    }
  }
  
  client.stop();
}


