/**
 * @file main.cpp
 * @author Mauricio Pereira (mauricio@ic.ufmt.br)
 * @brief 
 * @version 0.1
 * @date 2022-04-23
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <Arduino.h>

#include "main.h"

int ID_MQTT = 84;
extern "C" {
#include "user_interface.h"
}

/****************************************************
 * Bibliotecas de controle do Ar  
 * **************************************************/
#include <IRremoteESP8266.h>
#include <IRsend.h>

/*****************************************************
 *  Biblioteca de controle do prompt de comando
 * **************************************************/
#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>


/*****************************************************
 * Bibliotecas de MQTT e WiFi
 * **************************************************/
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include "DHT.h"

/*****************************************************
 * Bibliotecas de EEPROM
 * **************************************************/
#include <EEPROM.h>

// Objetos para parsear entrada via serial
CmdParser cmdParser;
CmdBuffer<64> cmdBuffer;
String bufferSerial = "buffer";
bool promptComandosAtivado = false;
unsigned long instanteInicial;
unsigned long instanteAtual;
const unsigned int timeoutInicializacao = 10000;


// armazena a situação de rede
int statusRede = WL_IDLE_STATUS; 
bool pisca = false;

bool flagSensorDispositivo = false;

long instanteUltimoEnvio = 0;  // TimeStamp da ultima mensagem enviada
const long TempoEspera = 5000;
int intervalo = 10000;   


/********************************************** 
 * Ajustes de conexão com a rede WiFi  
 *********************************************/ 
String ssid;
String pass;

/************************************************
 * Objeto e variávies para captura de dados do sensor de temperatura e umidade
 * **********************************************/
DHT dht(DHTPIN, DHTTYPE);
float humid;
float temp;
int intHumid;
int intTemp;

/********************************************************** 
 *    MQTT funções e variáveis
 *********************************************************/
const char* BROKER_MQTT = "192.168.0.105"; // ip/host do broker
int BROKER_PORT = 1883; // porta do broker
WiFiClient espClient; // Cria o objeto espClient
PubSubClient MQTT(espClient);
String topicoDispositivoConectado;
String topicoTemperaturaLab;
String topicoLuminosidadeLab;

/***********************************************************
 * Controles do sensor de luminosidade da sala
 **********************************************************/
float ldrValorSend;  
int ldrPin = A0;  //DEFINE O PINO NO QUAL O SENSOR DE LUMINOSIDADE LDR ESTARA CONECTADO ADC0 OU A0 NO NODEMCU
int ldrValor = 0; //VALOR LIDO DO SENSOR DE LUMINOSIDADE LDR
int limiarLuzLigada = 200;


/***********************************************************
 * Ajustes do controle de Ar Condicionado
 ***********************************************************/ 
  // INICIANDO TRANSMISSORES IR
  IRsend irsend1(AR_FRENTE_DIREITA);
  IRsend irsend2(AR_FRENTE_ESQUERDA);
  IRsend irsend3(AR_FUNDO_DIREITA);
  IRsend irsend4(AR_FUNDO_ESQUERDA);


/******************************************************************************************************************************
 *        FIM DO SETOR DE VARIÁVEIS GLOBAIS DO SISTEMA 
 * ****************************************************************************************************************************/


/*************************************************************************************
 * Inicializa conexão com MQTT
 *************************************************************************************/
void ConectarMQTT()
{
  IPAddress server(200, 129, 247, 243);
  MQTT.setServer(server, 1883);   //informa qual broker e porta deve ser conectado
  ReportarInfoUsuario(ERRO_MQTT);
  // Loop until we're reconnected
  while (!MQTT.connected()) {
    Serial.print("Tentando conectar ao broker ");
    Serial.println( BROKER_MQTT );
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (MQTT.connect(clientId.c_str())) {
      Serial.println("Conectado");  
      MQTT.setCallback(mqtt_callback);            //atribui função de callback (função chamada quando qualquer informação de um dos tópicos subescritos chega)  
      // conectando nos tópicos que o microcontrolador precisa esperar interação com o sistema MQTT
      MQTT.subscribe(topicoDispositivoConectado.c_str());
      MQTT.subscribe(topicoTemperaturaLab.c_str());
      MQTT.subscribe(topicoLuminosidadeLab.c_str());
    } 
    else {
      Serial.print("Falha, rc=");
      Serial.print(MQTT.state());
      Serial.println(" Tentando novamente em 5 segundos");
      // Wait 5 seconds before retrying
      delay(5000);
    }    
  }
  ReportarInfoUsuario( OK_MQTT );
}



/*************************************************************************************
 *  Callback para chegada de mensagens do broker
 *************************************************************************************/
void mqtt_callback(char* topic, byte* payload, unsigned int length)
{
  String msg;
  Serial.print("Message recebida [");
  Serial.print(topic);
  Serial.print("] ");
  //obtem a string do payload recebido
  for (unsigned int i = 0; i < length; i++)
  {
    char c = (char)payload[i];
    msg += c;
  }
  Serial.println(msg);
  TratarMsgRecebida(topic, msg );  
}


/*********************************************************************************
 * Tratamento de mensagens do MQTT
 * ******************************************************************************/
void TratarMsgRecebida(String topic, String msg )
{
  if (( topic == topicoDispositivoConectado ) && (msg == "não"))
   {
      Serial.println("Nova mensagem publicada Temp");
      // publicando que está ativo      
      MQTT.publish(topicoDispositivoConectado.c_str(), "sim");
      ReportarInfoUsuario(ENVIANDO_INFO);
      flagSensorDispositivo = true;
      
      ReportarInfoUsuario(DESLIGAR_LED);
   }

   /*
   if ( (topic = topic ) && (msg == "não")) 
     {
      Serial.println("Nova mensagem publicada Umid");
      // publicando que está ativo      
      MQTT.publish(topicoConectadoUmid.c_str(), "sim");
      ReportarInfoUsuario(ENVIANDO_INFO);
      flagSensorTemp1 = true;
      
      ReportarInfoUsuario(DESLIGAR_LED);
      
   } */
}


/**************************************************************************************
 * Função para teste e reconexão com o servidor do MQTT
 * ***********************************************************************************/
void reconnectMQTT() {
/*  while (!MQTT.connected()) {
    Serial.println("Tentando se conectar ao Broker MQTT: " + String(BROKER_MQTT));
    if (MQTT.connect("ESP8266-ESP12-E")) {
      Serial.println("Conectado");
      MQTT.subscribe("DZ/esp8266-01/led");

    } else {
      Serial.println("Falha ao Reconectar");
      Serial.println("Tentando se reconectar em 2 segundos");
      delay(2000);
    }
  }*/
}




//const char* ssid = "CAP";
//const char* password = "arduino2017";

///const char* ssid = "Coelhinho";
///const char* password = "52686719";

/*
IPAddress ip(192,168,0,182); 
IPAddress gateway(192,168,0,254);
IPAddress subnet(255,255,255,0);
 
WiFiServer server(80);
*/

//PubSubClient MQTT(espClient); // instancia o mqtt
//  WiFiClient espClient = server.available();
//  PubSubClient MQTT(espClient);

  





  



//Nunca execute nada na interrupcao, apenas setar flags!
/*
void tCallback(void *tCall){
    _timeout = true;
}

void usrInit(void){
    os_timer_setfn(&mTimer, tCallback, NULL);
    os_timer_arm(&mTimer, 5000, true);
}
*/

/************************************************************************
 * Função de ajustes de configurações do microcontrolador
 * **********************************************************************/
void setup(){
  // iniciando serial e envio de mensagens para acompanhamento do usuário
  Serial.begin(9600);
  Serial.println("Iniciando sistema de controle do ar");
  delay(10);
  // mostrando versão do sistema para o usuário
  MsgInicializacao(); 
  // mostrando menu de help ao usuário
  help();
  // ativando a verificação do tipo de inicialização e de ajustes
  MsgTipoInicializacao();
  //LoadConfigsEEPROM();
  // ativando ar condicionado
  Serial.println(F("Iniciando transmissores do ar condicionado"));
  irsend1.begin();
  irsend1.sendRaw(Desligar, 197,38);  
  Serial.println("Comando enviado: Desliga");  
  /*
  irsend2.begin();
  irsend3.begin();
  irsend4.begin();
  */
  /*
  LoadConfigsEEPROM() ; 
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  Serial.println(F("Iniciando Conexão WiFi"));
  ConectarWiFi();
  ReportarInfoUsuario(ERRO_MQTT);
  Serial.println(F("Iniciando MQTT"));
  Serial.print(F("Topico de verificação online é "));
  Serial.println(topicoDispositivoConectado);
    
  ConectarMQTT();
//  MQTT.publish(topicoTemperatura.c_str(),"30");
  MQTT.publish(topicoDispositivoConectado.c_str(), "sim");
  
  ReportarInfoUsuario(OK_MQTT);

  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("DHT22 Iniciado!");
  dht.begin();  // Inicia sensor de temperatura e humidade DHTxx
  Serial.println("Interrupcao inciada"); 
  */
}

void loop() {
  PromptComandos();
  irsend1.sendRaw(Desligar, 197,38);  
  Serial.println("Comando enviado: Desliga");
  delay(10000);
  irsend1.sendRaw(Ligar20C, 197,38);
  Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
  delay(10000);

/*  if (!MQTT.connected()) {
    reconnectMQTT();
  }

//========================================================================================================
  //Temperatura, Umidade e Luminosidade
  if (_timeout)
  {
    //Luminosidade
    ldrValor = analogRead(ldrPin); //O VALOR LIDO SERA ENTRE 0 E 1023
    ldrValor = map(ldrValor, 0, 1023, 100, 0);
    Serial.print("Luminosidade: ");
    Serial.print(ldrValor);
    Serial.print("% ");

    ldrValorSend = (float)ldrValor;
    ldrValorSend = ldrValorSend / 100;
    //Serial.print(ldrValorSend);

    //Temperatura, Umidade
    humid = dht.readHumidity();
    temp = dht.readTemperature();
    if (isnan(temp) || isnan(humid)) 
    {
      Serial.println("Failed to read from DHT");
    } 
    else
    {
      Serial.print("Umidade: ");
      intHumid = (int)humid;
      Serial.print(intHumid);
      Serial.print("% ");
      Serial.print("Temperatura: ");
      intTemp = (int)temp;
      Serial.print(intTemp);
      Serial.println("*C");
    }
    _timeout = false;
  }

  //======================================================================================================
  //Web Requests
  WiFiClient client = server.available();
  
  if (!client) {
    return;
  }
  
  String req = client.readStringUntil('\r');
  Serial.println(req);

  client.flush();

  String buf = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";

  client.print(buf);

  client.flush();

  if (req.indexOf("Temperatura") != -1)
  {
    client.flush();
    client.print(intTemp);
  }
  else  if (req.indexOf("Umidade") != -1)
  {
    client.flush();
    client.print(intHumid);
  }
  else if (req.indexOf("Luminosidade") != -1)
  {
    client.flush();
    client.print(ldrValorSend);
  }
  else if (req.indexOf("LigarL1") != -1)
  {
    if (ligado1)
    {
      digitalWrite(Lamp1, 1);
      ligado1 = false;
      client.flush();
    }
    else
    {
      digitalWrite(Lamp1, 0);
      ligado1 = true;
      client.flush();
    }
  }
  else if (req.indexOf("LigarL2") != -1)
  {
    if (ligado2)
    {
      digitalWrite(Lamp2, 1);
      ligado2 = false;
      client.flush();
    }
    else
    {
      digitalWrite(Lamp2, 0);
      ligado2 = true;
      client.flush();
    }
  }
  else if (req.indexOf("LigarL3") != -1)
  {
    if (ligado3)
    {
      digitalWrite(Lamp3, 1);
      ligado3 = false;
      client.flush();
    }
    else
    {
      digitalWrite(Lamp3, 0);
      ligado3 = true;
      client.flush();
    }
  }
  else if (req.indexOf("LigarL4") != -1)
  {
    if (ligado4)
    {
      digitalWrite(Lamp4, 1);
      ligado4 = false;
      client.flush();
    }
    else
    {
      digitalWrite(Lamp4, 0);
      ligado4 = true;
      client.flush();
    }
  }
  //Sinais AR CONDICIONADO
  //AR1=======================================================================
  else if (req.indexOf("Ar1") != -1)
  {
    if (req.indexOf("Ar1Desligar") != -1)
    {
      irsend1.sendRaw(Desligar, 197,38);  
      Serial.println("Comando enviado: Desliga");
    }
    else if (req.indexOf("Ar1Ligar") != -1)
    { 
      irsend1.sendRaw(Ligar20C, 197,38);
      Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
    }
    else if (req.indexOf("Ar1Temp16C") != -1)
    { 
      irsend1.sendRaw(Temp16C, 197,38);
      Serial.println("Comando enviado: Temperatura a 16°C");
    }
    else if (req.indexOf("Ar1Temp17C") != -1)
    { 
      irsend1.sendRaw(Temp17C, 197,38);
      Serial.println("Comando enviado: Temperatura a 17°C");
    }
    else if (req.indexOf("Ar1Temp18C") != -1)
    { 
      irsend1.sendRaw(Temp18C, 197,38);
      Serial.println("Comando enviado: Temperatura a 18°C");
    }
    else if (req.indexOf("Ar1Temp19C") != -1)
    { 
      irsend1.sendRaw(Temp19C, 197,38);
      Serial.println("Comando enviado: Temperatura a 19°C");
    }
    else if (req.indexOf("Ar1Temp20C") != -1)
    { 
      irsend1.sendRaw(Temp20C, 197,38);
      Serial.println("Comando enviado: Temperatura a 20°C");
    }
  }
  //AR2=======================================================================
    else if (req.indexOf("Ar2") != -1)
  {
    if (req.indexOf("Ar2Desligar") != -1)
    {
      irsend2.sendRaw(Desligar, 197,38);  
      Serial.println("Comando enviado: Desliga");
    }
    else if (req.indexOf("Ar2Ligar") != -1)
    { 
      irsend2.sendRaw(Ligar20C, 197,38);
      Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
    }
    else if (req.indexOf("Ar2Temp16C") != -1)
    { 
      irsend2.sendRaw(Temp16C, 197,38);
      Serial.println("Comando enviado: Temperatura a 16°C");
    }
    else if (req.indexOf("Ar2Temp17C") != -1)
    { 
      irsend2.sendRaw(Temp17C, 197,38);
      Serial.println("Comando enviado: Temperatura a 17°C");
    }
    else if (req.indexOf("Ar2Temp18C") != -1)
    { 
      irsend2.sendRaw(Temp18C, 197,38);
      Serial.println("Comando enviado: Temperatura a 18°C");
    }
    else if (req.indexOf("Ar2Temp19C") != -1)
    { 
      irsend2.sendRaw(Temp19C, 197,38);
      Serial.println("Comando enviado: Temperatura a 19°C");
    }
    else if (req.indexOf("Ar2Temp20C") != -1)
    { 
      irsend2.sendRaw(Temp20C, 197,38);
      Serial.println("Comando enviado: Temperatura a 20°C");
    }
  }
  //AR3=======================================================================
    else if (req.indexOf("Ar3") != -1)
  {
    if (req.indexOf("Ar3Desligar") != -1)
    {
      irsend3.sendRaw(Desligar, 197,38);  
      Serial.println("Comando enviado: Desliga");
    }
    else if (req.indexOf("Ar3Ligar") != -1)
    { 
      irsend3.sendRaw(Ligar20C, 197,38);
      Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
    }
    else if (req.indexOf("Ar3Temp16C") != -1)
    { 
      irsend3.sendRaw(Temp16C, 197,38);
      Serial.println("Comando enviado: Temperatura a 16°C");
    }
    else if (req.indexOf("Ar3Temp17C") != -1)
    { 
      irsend3.sendRaw(Temp17C, 197,38);
      Serial.println("Comando enviado: Temperatura a 17°C");
    }
    else if (req.indexOf("Ar3Temp18C") != -1)
    { 
      irsend3.sendRaw(Temp18C, 197,38);
      Serial.println("Comando enviado: Temperatura a 18°C");
    }
    else if (req.indexOf("Ar3Temp19C") != -1)
    { 
      irsend3.sendRaw(Temp19C, 197,38);
      Serial.println("Comando enviado: Temperatura a 19°C");
    }
    else if (req.indexOf("Ar3Temp20C") != -1)
    { 
      irsend3.sendRaw(Temp20C, 197,38);
      Serial.println("Comando enviado: Temperatura a 20°C");
    }
  }
  //AR4=======================================================================
    else if (req.indexOf("Ar4") != -1)
  {
    if (req.indexOf("Ar4Desligar") != -1)
    {
      irsend4.sendRaw(Desligar, 197,38);  
      Serial.println("Comando enviado: Desliga");
    }
    else if (req.indexOf("Ar4Ligar") != -1)
    { 
      irsend4.sendRaw(Ligar20C, 197,38);
      Serial.println("Comando enviado: Liga a uma temperatura de 20°C");
    }
    else if (req.indexOf("Ar4Temp16C") != -1)
    { 
      irsend4.sendRaw(Temp16C, 197,38);
      Serial.println("Comando enviado: Temperatura a 16°C");
    }
    else if (req.indexOf("Ar4Temp17C") != -1)
    { 
      irsend4.sendRaw(Temp17C, 197,38);
      Serial.println("Comando enviado: Temperatura a 17°C");
    }
    else if (req.indexOf("Ar4Temp18C") != -1)
    { 
      irsend4.sendRaw(Temp18C, 197,38);
      Serial.println("Comando enviado: Temperatura a 18°C");
    }
    else if (req.indexOf("Ar4Temp19C") != -1)
    { 
      irsend4.sendRaw(Temp19C, 197,38);
      Serial.println("Comando enviado: Temperatura a 19°C");
    }
    else if (req.indexOf("Ar4Temp20C") != -1)
    { 
      irsend4.sendRaw(Temp20C, 197,38);
      Serial.println("Comando enviado: Temperatura a 20°C");
    }
  }
  
  client.stop();
  */
}


/*********************************************************************************************
 *  Rotina para mostrar a porta serial funcionando e 
 *  as informações do software para o usuario
*********************************************************************************************/
void MsgInicializacao()
{
  delay(2000);
  Serial.println(Separador);
  Serial.println(Firmware);
  Serial.println(Versao);
  Serial.println(Responsavel);
  Serial.println(Separador);
  delay(3000);
}


void MsgTipoInicializacao()
{ 
  Serial.println(Separador);
  Serial.println(F("Digite 'SETUP' se desejar em modo de setup do equipamento"));
  Serial.println(F("Aguardando comandos da porta serial por 10 segundos")) ;
  instanteInicial = millis();
  instanteAtual = millis();
  cmdParser.setOptKeyValue(true);
  Serial.println("Iniciando timeout");
  cmdBuffer.clear();
  
  // aguardando usuario digitar s ou S  
  while ((instanteAtual - instanteInicial) < timeoutInicializacao ) //20000)
  {
    cmdBuffer.readFromSerial(&Serial,5000);
    instanteAtual = millis();
    Serial.print("Tamanho do buffer lido: ");
    bufferSerial = cmdBuffer.getStringFromBuffer();
    Serial.println( bufferSerial );
    if (bufferSerial.length() > 0)
    {
      Serial.print("Comando Lido: ");
      Serial.println(cmdBuffer.getStringFromBuffer());
      bufferSerial = cmdBuffer.getStringFromBuffer();
      promptComandosAtivado = tratarEntradaSerial();
    }
  } 
}


void PromptComandos()
{
  while (promptComandosAtivado == true)
  {
   cmdBuffer.readFromSerial(&Serial,5000);
    instanteAtual = millis();
    Serial.print("Tamanho do buffer lido: ");
    bufferSerial = cmdBuffer.getStringFromBuffer();
    Serial.println( bufferSerial );
    if (bufferSerial.length() > 0)
    {
      Serial.print("Comando Lido: ");
      Serial.println(cmdBuffer.getStringFromBuffer());
      bufferSerial = cmdBuffer.getStringFromBuffer();
      promptComandosAtivado = tratarEntradaSerial();
    }
          
  }
}

/**
 * @brief Trata os comandos 
 * 
 * @return true 
 * @return false 
 */
bool tratarEntradaSerial()
{
  String tmpMsg; 
  Serial.println(F("tratando dados da Serial"));
  // Parando timer para tratar informaçoes que estao sendo recebidas
  // -> não precisa.stop();
  // parando a interrupção
  //  MostrarInstanteAtual();
  if (cmdParser.parseCmd(cmdBuffer.getStringFromBuffer()) != CMDPARSER_ERROR) 
  {
    Serial.print("Comando identificado: ");
    Serial.println(cmdParser.getCommand());
    Serial.print("Quantidade de parâmetros: ");
    Serial.println(cmdParser.getParamCount());
  }  
  else if (cmdParser.equalCommand_P(PSTR("HELP")))
  {
    Serial.println("Ajuda ");
    help();
  }  
  else if ( cmdParser.equalCommand_P(PSTR("STATUSWIFI")))
  {
    Serial.println(F("Exibindo status da rede WiFi"));
    PrintWiFiConfigs();
    Serial.println();
    Serial.print(F("Tempo desde o ultimo pacote: "));
    Serial.print((millis() - instanteUltimoEnvio)/1000.0);
    Serial.println(F("s"));    
  }
  else if (cmdParser.equalCommand_P(PSTR("SET")))
  {
    Serial.println(F("Função de ajuste de configuração"));
  // avaliando o parâmetro de ajuste de SETUP
    tmpMsg = cmdParser.getValueFromKey("SETUP"); 
    Serial.print("Setup tmpMsg:");
    Serial.println(tmpMsg);
    if (tmpMsg.length() > 0) 
    {  
      if (tmpMsg == "ON")
      {
        Serial.print(F("Modo de configuração ativado "));  
        return true;
      }
      else if (tmpMsg == "OFF")
      {
        Serial.print(F("Modo de configuração desativado "));  
        return false;
      }      
    }
    // avaliando o parâmetro de ajuste de ssid
    tmpMsg = cmdParser.getValueFromKey("WIFISSID"); 
    Serial.print("tmpMsg:");
    Serial.println(tmpMsg);
    if (tmpMsg.length() > 0) 
    {  
      Serial.print(F("Ajustando SSID da Rede sem Fio: "));
      Serial.println(tmpMsg);
      SaveConfigEEPROM("SSID", tmpMsg);
    }    
    // avaliando o parâmetro de ajuste do pass da rede
    tmpMsg =  cmdParser.getValueFromKey_P(PSTR("WIFIPASS")); 
    Serial.print("tmpMsg:");
    Serial.println(tmpMsg);
    if (tmpMsg.length() > 0) 
    {  
      Serial.print(F("Ajustando Pass da Rede sem Fio: "));
      Serial.println(tmpMsg);
      SaveConfigEEPROM("PASS", tmpMsg);
    }
    // avaliando o parâmetro de ajuste do id do equipamento  
    Serial.println(cmdParser.getValueFromKey("ID"));
    Serial.print("tmpMsg:");
    tmpMsg = cmdParser.getValueFromKey_P(PSTR("ID"));
    Serial.println(tmpMsg);
    if (tmpMsg.length() > 0) 
    {  
      Serial.print(F("Ajustando Id do equipmamento: "));
      Serial.println(tmpMsg);
      SaveConfigEEPROM("ID", tmpMsg);
    }
    // avaliando o parametro de ajuste de temperatura do lab
    tmpMsg = cmdParser.getValueFromKey("TEMP");
    Serial.print("tmpMsg:");
    Serial.println(tmpMsg);
    if (tmpMsg.length() > 0) 
    {  
      Serial.print(F("Ajustando Temperatura do Lab : "));
      Serial.println(tmpMsg);
      // ajustar equipamento para transmitir via led ir a temperatura do lab
    }        
  }
  else if ( cmdParser.equalCommand_P(PSTR("RESET")))
  {
    Serial.print(F("Reset da memória"));
    EEPROM.begin(512);
    for (int i = 0 ; i < 512; i++)
      EEPROM.write(i, 0);
    Serial.println("Finalizado o ajuste o reset da EEPROM");
  }
  else 
  {
    Serial.println(F("Comando não reconhecido"));  
  }
  cmdBuffer.clear() ; 
  // zerando buffer a cada comando
  bufferSerial = "";  
  return true;
}


/*************************************************************************************
 * Funçao para tratar os dados recebidos pela porta serial
 *************************************************************************************/
void serialEvent() {
  /*char inChar;
  //Serial.print("Analisando comando\n");
  while (Serial.available()) {
    // get the new byte:
    inChar = (char)Serial.read();
    // add it to the inputString:
    bufferSerial = bufferSerial + String(inChar);
    // Caractere @ inicia modo de prompt de comandos
    if ( (inChar == 10 ) || (inChar == 13) || (inChar == '#')) {
      Serial.print("bufferSerial: ");
      Serial.println(bufferSerial);
      tratarEntradaSerial();
    }
  }*/
  cmdBuffer.readFromSerial(&Serial,0);
  Serial.print("Comando Lido: ");
  Serial.print(cmdBuffer.getStringFromBuffer());
  bufferSerial = cmdBuffer.getStringFromBuffer();
  tratarEntradaSerial();
}

/********************************************************************************************
 *  Lendo configuracoes da EEPROM
 ********************************************************************************************/
void LoadConfigsEEPROM()
{
  EEPROM.begin(512);
  int temp = EEPROM.read(EEPROM_ID);
  ID_MQTT = temp;
  Serial.print("Id lido: ");
  Serial.println(ID_MQTT);

  //topicoConectado = "IC_Sala" + String(ID_MQTT) + "_online";
  //topicoTemperatura = "IC_Sala" + String(ID_MQTT) + "_Temp1"; 
  /*
  topicoConectadoTemp = "PoP_Sala" + String(ID_MQTT) + "_Temp1_online";
  topicoTemperatura = "PoP_Sala" + String(ID_MQTT) + "_Temp1"; 
*/
  
 // novos topicos de Umidade
 /*
 topicoConectadoUmid =  "PoP_Sala" + String(ID_MQTT) + "_Umid1_online";
 topicoUmidade = "PoP_Sala" + String(ID_MQTT) + "_Umid1"; 
 */
  int tamString = EEPROM.read(EEPROM_SSID);
  for (int i = 1; i <= tamString; i++)
  {
    ssid += (char) EEPROM.read(EEPROM_SSID + i);
  }
  Serial.println(pass);

  tamString = EEPROM.read(EEPROM_PASS);
  Serial.print("Tamanho string pass: ");
  Serial.println(tamString);
  for (int i = 1; i <= tamString; i++)
  {
    pass += (char) EEPROM.read(EEPROM_PASS + i);
  }
  Serial.println(ssid);
  EEPROM.end();
}

/********************************************************************************************
 *  Salvando configurações na EEPROM
 ********************************************************************************************/
void SaveConfigEEPROM( String key, String value)
{
  int id;
  EEPROM.begin(512);
  if (key == "ID"){
      id = value.toInt();
      Serial.print("Id lido do parâmetro: ");
      Serial.println(id);
      EEPROM.write(EEPROM_ID, id );      
  }
  else if  (key == "SSID")
  {
    EEPROM.write(EEPROM_SSID, value.length() );
    for (unsigned int i = 1 ; i <= value.length(); i++ )
      EEPROM.write(EEPROM_SSID+i, (char) value[i-1]);

  }
  else if  (key == "PASS")
  {
    EEPROM.write(EEPROM_PASS, value.length() );
    for (unsigned int i = 1 ; i <= value.length(); i++ )
      EEPROM.write(EEPROM_PASS+i, (char) value[i-1]);      
  } 
  EEPROM.end();
}





/*********************************************************************************************
 * Rotina de conexão e status da conexão
*********************************************************************************************/

void ConectarWiFi()
{
  int contador=1;
  ReportarInfoUsuario(DISCONECTADO);
  Serial.print(F("Conectando na rede WiFi "));
  Serial.print(ssid);
  Serial.print(F(" "));
  Serial.println("...");
  Serial.flush();
  while (statusRede != WL_CONNECTED )
  {    
    Serial.print(".");
    Serial.flush();
    pisca = !pisca;
    statusRede = WiFi.begin(ssid, pass);
    Serial.print("Tentativa ");
    Serial.println(contador);
    contador++;
    Serial.flush();    
    delay(15000);
  }
  Serial.println("Saiu");
  Serial.print(F("Conectado a rede "));
  Serial.println(ssid);
  PrintWiFiConfigs();  
  ReportarInfoUsuario(CONECTADO);
}

/*************************************************************************************
 * Mostra as configurações de rede, da placa e qualidade de sinal
 *************************************************************************************/
void PrintWiFiConfigs()
{
  byte mac[6];
  Serial.println(F("###### WiFi Configs #######"));
  Serial.print(F("IP: "));
  Serial.println(WiFi.localIP());
  Serial.print(F("MAC: "));
  WiFi.macAddress(mac);
  Serial.print(mac[5], HEX);
  Serial.print(":");
  Serial.print(mac[4], HEX);
  Serial.print(":");
  Serial.print(mac[3], HEX);
  Serial.print(":");
  Serial.print(mac[2], HEX);
  Serial.print(":");
  Serial.print(mac[1], HEX);
  Serial.print(":");
  Serial.println(mac[0], HEX);

  IPAddress subnet = WiFi.subnetMask();
  Serial.print("NetMask: ");
  Serial.println(subnet);

  IPAddress gateway = WiFi.gatewayIP();
  Serial.print("Gateway: ");
  Serial.println(gateway);

  Serial.println();
  Serial.print(F("Rede: "));
  Serial.println(WiFi.SSID());

  Serial.print(F("RSSI: "));
  Serial.println(WiFi.RSSI());
  Serial.println(F("###### WiFi Configs #######"));  
}


/********************************************************************************************
 *  Usa o Led para informar os usuários a situação atual
 ********************************************************************************************/
void ReportarInfoUsuario( TStatusLed status )
{
  Serial.println("Reporta ativado sem mensagem");
  /*
  switch (status)
  {
  case INICIANDO : 
  //Apaga o display
  display.clear();
  display.setFont(ArialMT_Plain_10);  
  display.setTextAlignment(TEXT_ALIGN_CENTER);  
  display.drawString(64, 2, "Instituto de Computação");  
  display.drawXbm(40,16,48,48,logo48_bits);
  display.display();
  delay(6000);

  display.clear();
  display.setFont(ArialMT_Plain_10);  
  display.setTextAlignment(TEXT_ALIGN_CENTER);  
  display.drawString(64, 2, "CAP - IC - UFMT");  
  display.drawXbm(40,16,48,48,logoCAP48_bits);
  display.display();  
  delay(4000);  
  display.clear();
    //Atualiza informacoes de inicialização
  display.setFont(ArialMT_Plain_10);  
  display.setTextAlignment(TEXT_ALIGN_CENTER);  
  display.drawString(64, 38, F("Iniciando")); 
  display.drawString(64, 50, F("Aguardando serial"));
  SetBarraStatus(20);
  
  break;
    
  case DISCONECTADO:  
    //Apaga o display
    display.clear();    
    SetBarraStatus(40);
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 50, "Tentando conectar Wi-Fi");     
    delay(1000); 
    break;

  case CONECTADO:  
    display.clear();
    SetBarraStatus(60);
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 50, "Conectado a rede ");      
    delay(500);
    break;

case ERRO_MQTT:
    display.clear();
    SetBarraStatus(80);
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 50, "Iniciando MQTT");
    
   delay(500) ; 
   break;

case OK_MQTT:

    display.clear();
    SetBarraStatus(90);
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(64, 50, "Serviço conectado"); 
    delay(1000); 
    break;

case FIM_INICIALIZACAO:
      display.clear();
      SetBarraStatus(100);
      delay(1000);
      display.drawString(64, 50, "Dispositivo Iniciado"); 
      
      
      break;

  case LENDO_SENSOR: 
    display.clear();
    
    for (int i = 0; i <= 100 ; i += 10)
    {
      SetBarraStatus(i);
      display.setFont(ArialMT_Plain_10);
      display.setTextAlignment(TEXT_ALIGN_CENTER);
      display.drawString(64, 50, "Lendo sensor ...");       
      delay(100);      
      display.display();
    }
    
    break;

  case ENVIANDO_INFO:
    
      display.clear();
       //Desenha as molduras
      display.drawRect(0, 0, 128, 16);
       //Atualiza informacoes da temperatura
      display.setFont(ArialMT_Plain_10);
      display.setTextAlignment(TEXT_ALIGN_CENTER);
      String msgMolduraPrincipal = "Monitor da Sala " + String(ID_MQTT) ;
      display.drawString(64, 2, msgMolduraPrincipal.c_str());
      
      display.drawRect(0, 16, 128, 48);
      display.setFont(ArialMT_Plain_24);
      display.drawString(26, 26, String(TemperaturaAtual,1));
      display.drawCircle(52, 32, 2);

      display.drawLine(60, 16, 60, 63);
      display.drawRect(0, 16, 128, 48);
      display.setFont(ArialMT_Plain_24);
      display.drawString(96, 26, String(UmidadeAtual,0) + "%");      


      /*display.drawLine(67, 16, 67, 63);
      display.setTextAlignment(TEXT_ALIGN_CENTER);
      display.setFont(ArialMT_Plain_16);
      display.drawString(96, 20, "Média");      
      display.setFont(ArialMT_Plain_16);
      display.drawLine(67, 40, 128, 40);
      display.drawString(90, 43, String(MediaAtual,1));
      display.drawCircle(107, 45, 2);*/
    
    //break;

  

  //default:
  /*  SetLed(Desligado);*/
  //  break;
  //}   
  // display.display();
}

/*************************************************************************************
 * Mostra na serial os comandos disponveis
 *************************************************************************************/
void help()
{
  Serial.println(F("============================================================"));
  Serial.println(F("||                   Help de comandos                     ||"));
  Serial.println(F("============================================================"));
  Serial.println(F("|| set wifissid=<novoSSID>    |  Ajusta e salva novo SSID ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| set wifipass=<novoPass>    |  Ajusta e salva nova PASS ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| set id=<novoId>            |  Ajusta e salva novo ID   ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| set setup=ON               |  Liga o modo prompt       ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| set setup=OFF              |  Desliga o modo prompt    ||"));  
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| statusWiFi                 |  Mostra configs da rede   ||"));
  Serial.println(F("------------------------------------------------------------"));
  Serial.println(F("|| help                       |  Mostra help de comandos  ||"));
  Serial.println(F("============================================================"));
}


